package launch;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;

import javax.swing.JPanel;

public class Panel extends JPanel {
	Dimension ScrSize = Toolkit.getDefaultToolkit().getScreenSize();
public Panel(){
	setPreferredSize(ScrSize);
}
public void paintComponent(Graphics g2){
	Graphics2D g = (Graphics2D)g2;
	g.setColor(Color.WHITE);
	g.fillRect(0, 0, ScrSize.width, ScrSize.height);
	g.setColor(Color.RED.brighter());
	g.fillRect(ScrSize.width - 10, 0, 10, 10);
	g.setColor(Color.yellow.darker());
	g.fillRect(ScrSize.width - 20, 0, 10, 10);
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			g.drawOval(100*i+100,100*j+100,75,75);
		}
	}
}
}
